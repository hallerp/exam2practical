
// Exam 2 Practical
// Paul Haller
// Attempted the extra credit

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

float FindAverage(float numbers[], int amount)
{
	float sum = 0, average;

	for (int i = 0; i < amount; i++)
	{
		sum += numbers[i];
	}
	average = sum / amount;
	return average;
}

float FindMin(float numbers[], int amount)
{
	float min = numbers[0];
	for (int i = 0; i < amount; i++)
	{
		if (min > numbers[i])
		{
			min = numbers[i];
		}
	}
	return min;
}

float FindMax(float numbers[], int amount)
{
	float max = numbers[0];
	for (int i = 0; i < amount; i++)
	{
		if (max < numbers[i])
		{
			max = numbers[i];
		}
	}
	return max;
}


int main()
{
	const int amount = 5;
	float numbers[amount], reverse[amount];


	cout << "Enter 5 numbers\n\n";

	for (int i = 0; i < amount; i++)
	{
		cout << "Please enter number " << i+1 << "\n";
		cin >> numbers[i];
	}
	

	cout << "\nThe calculated average of the numbers is: " << FindAverage(numbers, amount) << "\n";
	cout << "The smallest number is: " << FindMin(numbers, amount) << "\n";
	cout << "The largest number is: " << FindMax(numbers, amount) << "\n\n";
	cout << "The numbers you entered in reverse order: \n";
	for (int i = amount-1; i >= 0; i--)
	{
		cout << numbers[i] << " ";
	}

	char save;
	cout << "\n\nWould you like to save the results to a text file? \"Y\" to save or anything else to exit: ";
	cin >> save;
	if (save == 'y')
	{
		string filepath = "Test.text";
		ofstream ofs(filepath);
		if (ofs)
		{
			ofs << "The calculated average of the numbers is: " << FindAverage(numbers, amount) << "\n";
			ofs << "The smallest number is: " << FindMin(numbers, amount) << "\n";
			ofs << "The largest number is: " << FindMax(numbers, amount) << "\n";
			ofs << "The numbers you entered in reverse order: \n";
			for (int i = amount - 1; i >= 0; i--)
			{
				ofs << numbers[i] << " ";
			}
		}
	}
	(void)_getch();
	return 0;
}